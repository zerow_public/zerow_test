from fastapi import FastAPI
import uvicorn
from fastapi.openapi.utils import get_openapi

app = FastAPI()


@app.post("/")
async def root():
    print("Hello World")
    return {"message": "Hello World"}


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Custom title",
        version="2.5.0",
        summary="This is a very custom OpenAPI schema",
        description="Here's a longer description of the custom **OpenAPI** schema",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


if __name__ == "__main__":
    print("Starting application...")
    # app.openapi = custom_openapi
    uvicorn.run(app, host="0.0.0.0", port=5000)
